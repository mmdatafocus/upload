package upload

import (
	"context"
	"io"
	"os"
	"strings"
)

// A FileStorage handles persisting uploaded files to file system.
// It saves to /uploads folder.
type FileStorage struct {
}

func (s *FileStorage) ensureDir() {
	path := "uploads"
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, os.ModePerm)
	}
}

// Save saves given stream to file system.
func (s *FileStorage) Save(ctx context.Context, fi *FileInfo, r io.Reader) error {
	s.ensureDir()
	w, err := os.Create("uploads/" + fi.Key)
	if err != nil {
		return err
	}
	io.Copy(w, r)

	// wait until flashed to disk
	return w.Sync()
}

// Get returns file stream from file system by given key
func (s *FileStorage) Get(ctx context.Context, key string) (*FileInfo, io.Reader, error) {
	keySlice := strings.Split(key, "_")

	fi := &FileInfo{
		Key:         key,
		Name:        keySlice[len(keySlice)-1],
		ContentType: "application/octet-stream",
	}

	f, err := os.Open("uploads/" + key)
	return fi, f, err
}

// Delete deletes file by given key
func (s *FileStorage) Delete(ctx context.Context, key string) error {
	s.ensureDir()
	return os.Remove("uploads/" + key)
}
