package upload

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
)

const (
	// MinFileSize is the min size of uploaded file in bytes
	MinFileSize = 1
	// MaxFileSize is the max size of uploaded file in bytes
	MaxFileSize = 2e+8 // 200 MB

	// MaxAge is cache expiration
	MaxAge = 3600
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// FileInfo represents uploaded file meta info.
type FileInfo struct {
	Crc32       string `json:"crc32"`
	Key         string `json:"key"`
	Name        string `json:"name"`
	ContentType string `json:"contentType"`
	Error       string `json:"error,omitempty"`
}

func validateSize(size int64) error {
	if size < MinFileSize {
		return errors.New("File is too small")
	} else if size > MaxFileSize {
		return errors.New("File is too big")
	} else {
		return nil
	}
}

func extractKey(r *http.Request) string {
	splits := strings.Split(r.URL.Path, "/")
	key, _ := url.PathUnescape(splits[len(splits)-1])
	return key
}

// A Storage handles persisting of uploaded file
type Storage interface {
	Save(ctx context.Context, fi *FileInfo, r io.Reader) error
	Get(ctx context.Context, key string) (*FileInfo, io.Reader, error)
	Delete(ctx context.Context, key string) error
}

func (h *Handler) handleUpload(r *http.Request, p *multipart.Part) (fi *FileInfo) {
	fi = &FileInfo{
		Name:        p.FileName(),
		ContentType: p.Header.Get("Content-Type"),
	}
	defer func() {
		if rec := recover(); rec != nil {
			log.Println(rec)
			fi.Error = rec.(error).Error()
		}
	}()
	hash := crc32.NewIEEE()
	var buffer bytes.Buffer
	mw := io.MultiWriter(&buffer, hash)
	lr := &io.LimitedReader{R: p, N: MaxFileSize + 1}
	_, err := io.Copy(mw, lr)
	check(err)
	size := MaxFileSize + 1 - lr.N
	if serr := validateSize(size); serr != nil {
		fi.Error = serr.Error()
		return
	}
	fi.Crc32 = base64.RawURLEncoding.EncodeToString([]byte(fmt.Sprint(hash.Sum32())))
	fi.Key = fi.Crc32 + "_" + base64.RawURLEncoding.EncodeToString([]byte(fi.Name))
	err = h.Storage.Save(r.Context(), fi, &buffer)
	check(err)
	return
}

func getFormValue(p *multipart.Part) string {
	var b bytes.Buffer
	io.CopyN(&b, p, int64(1<<20)) // Copy max: 1 MiB
	return b.String()
}

func (h *Handler) handleUploads(r *http.Request) (fileInfos []*FileInfo) {
	fileInfos = make([]*FileInfo, 0)
	mr, err := r.MultipartReader()
	check(err)
	r.Form, err = url.ParseQuery(r.URL.RawQuery)
	check(err)
	part, err := mr.NextPart()
	for err == nil {
		if name := part.FormName(); name != "" {
			if part.FileName() != "" {
				fileInfos = append(fileInfos, h.handleUpload(r, part))
			} else {
				r.Form[name] = append(r.Form[name], getFormValue(part))
			}
		}
		part, err = mr.NextPart()
	}
	return
}

// Handler handles upload which has compatible API with jQuery Upload.
type Handler struct {
	Storage Storage
}

func (h *Handler) get(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	key := extractKey(r)
	fi, img, err := h.Storage.Get(r.Context(), key)
	if err == nil {
		w.Header().Add("X-Content-Type-Options", "nosniff")
		w.Header().Add("Content-Disposition", "attachment; filename="+fi.Name)
		w.Header().Add("Content-Type", fi.ContentType)
		w.Header().Add(
			"Cache-Control",
			fmt.Sprintf("public,max-age=%d", MaxAge),
		)
		io.Copy(w, img)
		return
	}
	http.Error(w, "404 Not Found", http.StatusNotFound)
}

func (h *Handler) post(w http.ResponseWriter, r *http.Request) {
	result := make(map[string][]*FileInfo, 1)
	result["files"] = h.handleUploads(r)
	b, err := json.Marshal(result)
	check(err)
	w.Header().Set("Cache-Control", "no-cache")
	jsonType := "application/json"
	if strings.Index(r.Header.Get("Accept"), jsonType) != -1 {
		w.Header().Set("Content-Type", jsonType)
	}
	fmt.Fprintln(w, string(b))
}

func (h *Handler) delete(w http.ResponseWriter, r *http.Request) {
	key := extractKey(r)
	err := h.Storage.Delete(r.Context(), key)
	check(err)
	w.WriteHeader(200)
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	params, err := url.ParseQuery(r.URL.RawQuery)
	check(err)
	switch r.Method {
	case "OPTIONS", "HEAD":
		w.Header().Add(
			"Access-Control-Allow-Methods",
			"OPTIONS, HEAD, GET, POST, PUT, DELETE",
		)
		w.Header().Add(
			"Access-Control-Allow-Headers",
			"X-Requested-With, Content-Type, Content-Range, Content-Disposition",
		)
		return
	case "GET":
		h.get(w, r)
	case "POST":
		if len(params["_method"]) > 0 && params["_method"][0] == "DELETE" {
			h.delete(w, r)
		} else {
			h.post(w, r)
		}
	case "DELETE":
		h.delete(w, r)
	default:
		http.Error(w, "501 Not Implemented", http.StatusNotImplemented)
	}
}
