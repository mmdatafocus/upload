package cloud

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"mime"

	"bitbucket.org/mmdatafocus/upload"
	"cloud.google.com/go/storage"
	"google.golang.org/appengine/file"
)

// Storage handles persisting uploaded files to Google Cloud Storage.
type Storage struct {
}

type bucketClient struct {
	*storage.BucketHandle
	client *storage.Client
}

func (s *Storage) getBucket(ctx context.Context) (*bucketClient, error) {
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bucketName, err := file.DefaultBucketName(ctx)
	if err != nil {
		return nil, err
	}

	bucket := client.Bucket(bucketName)

	return &bucketClient{
		BucketHandle: bucket,
		client:       client,
	}, nil
}

// Save saves file to appengine default bucket
func (s *Storage) Save(ctx context.Context, fi *upload.FileInfo, r io.Reader) error {
	bucket, err := s.getBucket(ctx)
	if err != nil {
		return err
	}
	defer bucket.client.Close()

	w := bucket.Object("uploads/" + fi.Key).NewWriter(ctx)
	w.ContentDisposition = "attachment; filename=" + fi.Name
	w.ContentType = fi.ContentType

	_, err = io.Copy(w, r)
	if err != nil {
		return err
	}

	return w.Close()
}

// Get retrieves file from appengine default bucket
func (s *Storage) Get(ctx context.Context, key string) (*upload.FileInfo, io.Reader, error) {
	bucket, err := s.getBucket(ctx)
	if err != nil {
		return nil, nil, err
	}
	defer bucket.client.Close()
	bobj := bucket.Object("uploads/" + key)
	attrs, err := bobj.Attrs(ctx)
	if err != nil {
		return nil, nil, err
	}
	_, params, err := mime.ParseMediaType(attrs.ContentDisposition)
	fi := &upload.FileInfo{
		ContentType: attrs.ContentType,
		Name:        params["filename"],
		Key:         key,
	}

	rc, err := bobj.NewReader(ctx)
	if err != nil {
		return nil, nil, err
	}
	defer rc.Close()

	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, nil, err
	}

	return fi, bytes.NewBuffer(data), nil
}

// Delete deletes file from appengine default bucket
func (s *Storage) Delete(ctx context.Context, key string) error {
	bucket, err := s.getBucket(ctx)
	if err != nil {
		return err
	}
	defer bucket.client.Close()

	return bucket.Object("uploads/" + key).Delete(ctx)
}
